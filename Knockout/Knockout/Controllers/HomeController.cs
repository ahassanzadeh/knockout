﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knockout.Models;

namespace Knockout.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult V2()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult V3()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult V4()
        {
            ViewBag.Message = ".";

            return View();
        }

        public ActionResult Tasks()
        {
            List<TaskDetails> tasks = new List<TaskDetails>();
            for (int i = 0; i < 10; i++)
            {
                TaskDetails newTask = new TaskDetails
                {
                    Id = i,
                    Title = "Task " + (i + 1),
                    Details = "Task Details " + (i + 1),
                    Starts = DateTime.Now,
                    Ends = DateTime.Now.AddDays(i + 1)
                };
                tasks.Add(newTask);
            }
            return View(tasks);
        }

        public ActionResult Create()
        {
            throw new NotImplementedException();
        }
    }
}
