﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />
var k4xaml;
(function (k4xaml) {
    var IndexViewModel = (function () {
        function IndexViewModel() {
            var _this = this;
            //public DisplayMessage: KnockoutObservable<string> = ko.observable("hello devs");
            //public DisplayValue: KnockoutObservable<boolean> = ko.observable(true);
            //public DisplayMessage: KnockoutObservableArray = ko.observableArray([]);
            //constructor() {
            //    this.DisplayMessage.push({ Message: 'Message 1' });
            //    this.DisplayMessage.push({ Message: 'Message 2' });
            //    this.DisplayMessage.push({ Message: 'Message 3' });
            //    this.DisplayMessage.push({ Message: 'Message 4' });
            //}
            this.FirstName = ko.observable("");
            this.LastName = ko.observable("");
            this.FullName = ko.computed(function () {
                return _this.FirstName() + " " + _this.LastName();
            });
        }
        return IndexViewModel;
    })();
    k4xaml.IndexViewModel = IndexViewModel;
})(k4xaml || (k4xaml = {}));
//# sourceMappingURL=IndexViewModel.js.map
