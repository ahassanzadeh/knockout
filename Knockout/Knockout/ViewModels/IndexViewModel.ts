﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />
module k4xaml {
    export class IndexViewModel {
        //public DisplayMessage: KnockoutObservable<string> = ko.observable("hello devs");
        //public DisplayValue: KnockoutObservable<boolean> = ko.observable(true);

        //public DisplayMessage: KnockoutObservableArray = ko.observableArray([]);
        //constructor() {
        //    this.DisplayMessage.push({ Message: 'Message 1' });
        //    this.DisplayMessage.push({ Message: 'Message 2' });
        //    this.DisplayMessage.push({ Message: 'Message 3' });
        //    this.DisplayMessage.push({ Message: 'Message 4' });
        //}

        public FirstName: KnockoutObservable<string> = ko.observable("");
        public LastName: KnockoutObservable<string> = ko.observable("");
        public FullName: KnockoutComputed<string>;
        constructor() {
            this.FullName = ko.computed(() => {
                return this.FirstName() + " " + this.LastName();
            });
        }
    }
}