﻿///// <reference path="../scripts/typings/jquery/jquery.d.ts" />
///// <reference path="../scripts/typings/knockout/knockout.d.ts" />
///// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />

//module k4xaml {
//    export class ModalViewModel {

//        ko.bindingHandlers.dialog = {
//            init: function(element, valueAccessor, allBindingsAccessor) {
//                var options = ko.utils.unwrapObservable(valueAccessor()) || {};
//                //do in a setTimeout, so the applyBindings doesn't bind twice from element being copied and moved to bottom
//                setTimeout(function() {
//                    options.close = function() {
//                        allBindingsAccessor().dialogVisible(false);
//                    };

//                    $(element).dialog(options);
//                }, 0);

//                //handle disposal (not strictly necessary in this scenario)
//                ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
//                    $(element).dialog("destroy");
//                });
//            },
//            update: function(element, valueAccessor, allBindingsAccessor) {
//                var shouldBeOpen = ko.utils.unwrapObservable(allBindingsAccessor().dialogVisible),
//                    $el = $(element),
//                    dialog = $el.data("uiDialog") || $el.data("dialog");

//                //don't call open/close before initilization
//                if (dialog) {
//                    $el.dialog(shouldBeOpen ? "open" : "close");
//                }
//            }
//        };
//    }

//    $(document).ready(function () {
//        var viewModel = {
//            label: ko.observable('dialog test'),
//            isOpen: ko.observable(false),
//            open: function () {
//                this.isOpen(true);
//            },
//            close: function () {
//                this.isOpen(false);
//            }
//        };

//        ko.applyBindings(viewModel);


//    });
//} 