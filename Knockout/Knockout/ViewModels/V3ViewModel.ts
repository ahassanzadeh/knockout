﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />

module k4xaml {
    export class V3ViewModel{
        //1.
        //public FirstName: KnockoutObservable<string> = ko.observable("azadeh");
        //public LastName: KnockoutObservable<string> = ko.observable("hassanzadeh");
        //public DisplayValue: KnockoutObservable<string> = ko.observable("");

        //public Save(data) {
        //    data.DisplayValue (data.FirstName() + " " + data.LastName());
        //}

        //2
        //private wasClicked: boolean = false;
        //public DisplayMessage: KnockoutObservable<string> = ko.observable("");

        //public SpanClicked() {
        //    this.wasClicked = !this.wasClicked;
        //    var message = this.wasClicked ? "Span was clicked" : "";
        //    this.DisplayMessage(message);
        //}

        //3
        //public Team1Score: KnockoutObservable<Number> = ko.observable(5);
        //public Team2Score: KnockoutObservable<Number> = ko.observable(4);
        //public Announcement: KnockoutObservable<string> = ko.observable("");

        //public AnnounceWinner( team1Score: number, team2Score: number) {
        //    var self = this;
        //    var message = "";
        //    if (team1Score > team2Score) {
        //        message = "Team1";
        //    }
        //    else if (team1Score < team2Score) {
        //        message = "Team2";
        //    }
        //    else if (team1Score == team2Score) {
        //        message = "Tie";
        //    }
        //    this.Announcement(message);
        //}

        //4
        //public FirstName: KnockoutObservable<string> = ko.observable("");
        //public IsValid: KnockoutComputed<boolean>;

        //constructor() {
        //    var self = this;
        //    this.IsValid = ko.computed(() => {
        //        return self.FirstName().length > 0;
        //    });
        //}

        //public Save() {
        //    var self = this;
        //    self.FirstName("");
        //}


        //5.
        //public Field1HasFocus: KnockoutObservable<boolean> = ko.observable(false)
        //public Field2HasFocus: KnockoutObservable<boolean> = ko.observable(false)
        //public Field3HasFocus: KnockoutObservable<boolean> = ko.observable(false)

        //6.
        HandleMouseDown() {
            console.log("Handle mouse down");
        }
        HandleMouseUp() {
            console.log("Handle mouse up");
        }
    }
} 