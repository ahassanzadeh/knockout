﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />
var k4xaml;
(function (k4xaml) {
    var V4ViewModel = (function () {
        function V4ViewModel() {
            this.Person = new Person();
            this.Person.FirstName("azadeh");
            this.Person.LastName("hassanzadeh");
            this.Person.Age(35);

            var address = new Address();
            address.Address("32 shorleine dr");
            address.City("Rhodes");
            address.State("NSW");
            address.Zip("2138");

            this.Person.Address(address);
        }
        V4ViewModel.prototype.SavePerson = function () {
            console.log("Saving Person");
        };
        return V4ViewModel;
    })();
    k4xaml.V4ViewModel = V4ViewModel;

    var Person = (function () {
        function Person() {
            this.FirstName = ko.observable("");
            this.LastName = ko.observable("");
            this.Age = ko.observable(0);
            this.Address = ko.observable(new Address());
        }
        return Person;
    })();
    k4xaml.Person = Person;
    var Address = (function () {
        function Address() {
            this.Address = ko.observable("");
            this.State = ko.observable("");
            this.City = ko.observable("");
            this.Zip = ko.observable("");
        }
        return Address;
    })();
    k4xaml.Address = Address;
})(k4xaml || (k4xaml = {}));
//# sourceMappingURL=V4ViewModel.js.map
