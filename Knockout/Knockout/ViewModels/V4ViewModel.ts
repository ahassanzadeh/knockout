﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/knockout.mapping/knockout.mapping.d.ts" />
module k4xaml {

    export class V4ViewModel {
        public Person: Person;

        constructor() {
            this.Person = new Person();
            this.Person.FirstName("azadeh");
            this.Person.LastName("hassanzadeh");
            this.Person.Age(35);

            var address = new Address();
            address.Address("32 shorleine dr");
            address.City("Rhodes");
            address.State("NSW");
            address.Zip("2138");

            this.Person.Address(address);
        }
        public SavePerson() {
            console.log("Saving Person");
        }

    }

    export class Person {
        public FirstName: KnockoutObservable<string> = ko.observable("");
        public LastName: KnockoutObservable<string> = ko.observable("");
        public Age: KnockoutObservable<number> = ko.observable(0);
        public Address: KnockoutObservable<Address> = ko.observable(new Address());

    }
    export class Address {
        public Address: KnockoutObservable<string> = ko.observable("");
        public State: KnockoutObservable<string> = ko.observable("");
        public City: KnockoutObservable<string> = ko.observable("");
        public Zip: KnockoutObservable<string> = ko.observable("");
    }
}